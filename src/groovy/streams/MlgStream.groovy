package streams

import org.codehaus.jackson.JsonNode

class MlgStream implements Stream {

    String getName() {
        'MLG'
    }

    JsonService jsonService = new JsonService()

    void update(Date index) {
        def streams = 1
        def offset = 0
        def perPage = 100

        def uri = "http://www.majorleaguegaming.com/api/channels/all.js?fields=id,name,slug,subtitle,stream_name,type,embed_code,description,subscription_url"
        print "GET [${name}] ${uri}"
        try {
            process(jsonService.fromJson(new URL(uri).text), index)
        }
        catch (Exception e) {
            print "Exception: ${e}: ${e.stackTrace.toString()}"
        }
    }

    Map<String, Long> streamViewers() {
        def m = new HashMap<String, Long>()

        try {
            def uri = "http://streamapi.majorleaguegaming.com/service/streams/all"
            def streams = jsonService.fromJson(new URL(uri).text).data.items
            for (JsonNode stream: streams) {
                def name = stream?.stream_name?.textValue ?: ""
                def viewers = stream?.viewers?.asLong()
                if (name && viewers) {
                    m[name] = viewers
                }
            }
        }
        catch (Exception e) {
            print "Exception: ${e}: ${e.stackTrace.toString()}"
        }
        return m
    }

    int process(json, date) {
        int count = 0
        def streams = json.data.items

        Map<String, Long> streamViewers = streamViewers()
        for (JsonNode stream: streams) {
            def ss = new StreamSnapshot(
                    date: date,
                    service: name,
                    channel: stream?.name?.textValue ?: "Unlisted",
                    game: "Unlisted",
                    broadcastName: stream?.description?.textValue?.take(254) ?: "",
                    viewers: streamViewers[stream?.stream_name?.textValue],
                    json: stream.toString(),
                    url: "http://www.majorleaguegaming.com/player/embed/${stream?.slug?.textValue}"
            )
            if (ss.viewers >= 1) {
                count++
                ss.save(failOnError: true)
            }
        }

        return count
    }
}
