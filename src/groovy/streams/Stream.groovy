package streams

interface Stream {

    String getName()
    void update(Date index)

}
