package streams

import org.codehaus.jackson.JsonNode

class AzubuStream implements Stream {

    String getName() {
        'Azubu'
    }

    void update(Date index) {

        JsonService jsonService = new JsonService()
        def streams = true
        def offset = 0
        def perPage = 100

        while (streams) {
            def uri = "http://api.azubu.tv/public/channel/live/list?limit=${perPage}&offset=${offset}"
            print "GET [${name}] ${uri}"
            try {
                streams = process(jsonService.fromJson(new URL(uri).text), index)
                offset += perPage
            }
            catch (Exception e) {
                print "Exception: ${e}: ${e.stackTrace.toString()}"
                streams = 0
            }
        }
    }

    Boolean process(json, date) {
        int count = 0
        def streams = json.data
        for (JsonNode stream: streams) {
            def ss = new StreamSnapshot(
                    date: date,
                    service: name,
                    channel: stream?.user?.username?.textValue ?: "Unlisted",
                    game: stream?.category?.name?.textValue ?: "Unlisted",
                    broadcastName: stream?.user?.username?.textValue ?: "Unlisted",
                    viewers: stream?.view_count?.asLong(),
                    json: stream.toString(),
                    url: stream?.url_channel?.textValue ?: ""
            )
            if (ss.viewers >= 1) {
                count++
                ss.save(failOnError: true)
            }
        }
        return false  // If there's more streams, must fix this logic
    }
}
