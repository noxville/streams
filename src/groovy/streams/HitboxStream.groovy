package streams

import org.codehaus.jackson.JsonNode

class HitboxStream implements Stream {

    String getName() {
        'Hitbox'
    }

    void update(Date index) {

        JsonService jsonService = new JsonService()

        def uri = "http://api.hitbox.tv/media/"
        print "GET [${name}] ${uri}"

        try {
            process(jsonService.fromJson(new URL(uri).text), index)
        }
        catch (Exception e) {
            print "Exception: ${e}: ${e.stackTrace.toString()}"
        }
    }

    void process(json, date) {
        def streams = json.livestream
        for (JsonNode stream: streams) {

            def ss = new StreamSnapshot(
                    date: date,
                    service: name,
                    channel: stream?.media_user_name?.textValue ?: "Unlisted",
                    game: stream?.category_name?.textValue ?: "Unlisted",
                    broadcastName: stream?.media_status?.textValue ?: "Unlisted",
                    viewers: stream?.media_views?.asLong(),
                    json: stream.toString(),
                    url: stream?.channel?.channel_link?.textValue ?: "Unlisted"
            )
            if (ss.viewers >= 1) {
                ss.save(failOnError: true)
            }
        }

    }
}
