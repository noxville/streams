package streams

import org.codehaus.jackson.JsonNode

class TwitchStream implements Stream {

    String getName() {
        'Twitch'
    }

    void update(Date index) {

        JsonService jsonService = new JsonService()
        def streams = 1
        def offset = 0
        def perPage = 100

        while (streams) {
            def uri = "https://api.twitch.tv/kraken/streams?limit=${perPage}&offset=${offset}"
            print "GET [${name}] ${uri}"
            try {
                streams = process(jsonService.fromJson(new URL(uri).text), index)
                offset += perPage
            }
            catch (Exception e) {
                print "Exception: ${e}: ${e.stackTrace.toString()}"
                streams = 0
            }
        }
    }

    int process(json, date) {
        int count = 0
        def streams = json.streams
        for (JsonNode stream: streams) {
            def ss = new StreamSnapshot(
                    date: date,
                    service: name,
                    channel: stream?.channel?.name?.textValue ?: "Unlisted",
                    game: stream?.game?.textValue ?: "Unlisted",
                    broadcastName: stream?.channel?.status?.textValue ?: "Unlisted",
                    viewers: stream?.viewers?.asLong(),
                    json: stream.toString(),
                    url: stream?.channel?.url?.textValue ?: "???"
            )
            if (ss.viewers >= 10) {
                count++
                ss.save(failOnError: true)
            }
        }

        return count
    }
}
