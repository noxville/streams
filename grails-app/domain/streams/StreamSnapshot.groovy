package streams

class StreamSnapshot {

    static transients = []

    Date date
    String service
    String channel
    String game
    String broadcastName
    Long viewers
    String json
    String url

    static constraints = {
        date(nullable: true, blank: true)
        service(nullable: true, blank: true)
        channel(nullable: true, blank: true)
        game(nullable: true, blank: true)
        broadcastName(nullable: true, blank: true)
        viewers()
        json(blank: true)
        url(null: true, blank: true)
    }

    static mapping = {
        json type: 'text'
    }


    @Override
    public String toString() {
        return "StreamSnapshot{" +
                "id=" + id +
                ", date=" + date +
                ", service='" + service + '\'' +
                ", channel='" + channel + '\'' +
                ", game='" + game + '\'' +
                ", broadcastName='" + broadcastName + '\'' +
                ", viewers=" + viewers +
                '}';
    }
}
