package streams

class HomeController {

    def streamUpdateService

    def index() {
        def streamUpdates = streamUpdateService.lastUpdate
        def streamTotals = streamUpdateService.currentTotals

        def streams = new ArrayList<StreamSnapshot>()
        for (Stream stream: streamUpdates.keySet()) {
            def update = streamUpdates[stream]
            streams.addAll(StreamSnapshot.findAllByDateAndService(update, stream.name))
        }
        streams = streams.sort { -it.viewers }

        [streamUpdates:streamUpdates, streamTotals:streamTotals, streams: streams]
    }

    def update() {
        streamUpdateService.update()

        redirect(action: 'index')
    }
}
