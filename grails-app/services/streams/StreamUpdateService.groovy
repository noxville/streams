package streams

import grails.transaction.Transactional
import javax.annotation.PostConstruct

@Transactional
class StreamUpdateService {

    List<Stream> streams = new ArrayList<>()
    Map<Stream, Date> lastUpdate = new HashMap<>()
    Map<Stream, Long> currentTotals = new HashMap<>()

    @PostConstruct
    def init() {
        print "Initializing StreamService"
        streams.add(new TwitchStream())
        streams.add(new HitboxStream())
        streams.add(new AzubuStream())
        streams.add(new MlgStream())
    }

    def update() {
        for (Stream stream: streams) {
            Date now = new Date()
            stream.update(now)
            lastUpdate[stream] = now
            Long total = (Long) StreamSnapshot.findAllByDateAndService(now, stream.name).sum { StreamSnapshot ss -> ss.viewers }
            currentTotals[stream] = total
        }

    }

}
