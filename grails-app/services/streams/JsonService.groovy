package streams

import org.codehaus.jackson.JsonNode
import org.codehaus.jackson.Version
import org.codehaus.jackson.map.DeserializationConfig
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.map.SerializationConfig
import org.codehaus.jackson.map.module.SimpleModule
import org.codehaus.jackson.map.ser.ToStringSerializer

/**
 * Helps convert objects to JSON using Jackson. Jackson is more flexible and faster than the Grails/Groovy built
 * in JSON converters.
 */
class JsonService {

    //--------------------------------

    static transactional = false

    //--------------------------------

    private ObjectMapper mapper = new ObjectMapper()

    //--------------------------------

    JsonService() {
        SimpleModule module = new SimpleModule("Pork", new Version(1, 0, 0, null));
        module.addSerializer(GString.class, ToStringSerializer.instance);
        mapper.registerModule(module)

        mapper.configure(SerializationConfig.Feature.WRITE_NULL_PROPERTIES, false)
        mapper.configure(SerializationConfig.Feature.WRITE_EMPTY_JSON_ARRAYS, false)
        mapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false)
        mapper.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false)
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    //--------------------------------

    /**
     * Converts an object into to JSON.
     */
    String toJson(Object o) {
        mapper.writeValueAsString(o)
    }

    /**
     * Converts content to an instance of a particular type.
     */
    public <T> T fromJson(String content, Class<T> klass) {
        mapper.readValue(content, klass)
    }

    /**
     * Converts a node to an instance of a particular type.
     */
    public <T> T fromJson(JsonNode node, Class<T> klass) {
        mapper.readValue(node, klass)
    }

    /**
     * Converts a json array into an array of a particular type.
     */
    public <T> List<T> fromJson(String content, Class collectionKlass, Class<T> klass) {
        mapper.readValue(content, mapper.getTypeFactory().constructCollectionType(collectionKlass, klass))
    }

    public <K, V> Map<K, V> fromJson(String content, Class mapKlass, Class<K> key, Class<V> value) {
        mapper.readValue(content, mapper.getTypeFactory().constructMapType(mapKlass, key, value))
    }

    public <T> List<T> fromJson(JsonNode node, Class collectionKlass, Class<T> klass) {
        mapper.readValue(node, mapper.getTypeFactory().constructCollectionType(collectionKlass, klass))
    }

    /**
     * Returns a JsonNode object for the given content. This is
     * essentially an arbitrary map object.
     */
    public JsonNode fromJson(String content) {
        mapper.readTree(content)
    }
}
