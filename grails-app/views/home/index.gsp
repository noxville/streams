<!doctype html>
<html>
<head>
    <meta name="layout" content="bootstrap"/>
    <title>Live Streams</title>
</head>

<body>
<div class="row-fluid">
    <aside id="application-status" class="span3">

        <div class="well" id="stream-updates">
            Last updates by stream: </br>
            <g:each in="${streamUpdates.keySet().asList().sort { streamUpdates[it].time * -1 }}" var="stream">
                ${stream.name}: <g:formatNumber number="${(new Date().time - streamUpdates[stream].time)/1000}" format="0"/> seconds ago. </br>
            </g:each>
        </div>

        <div class="well" id="platform-totals">
            Total by platform: </br>
            <g:each in="${streamTotals.keySet().asList().sort { streamTotals[it] * -1 }}" var="stream">
                ${stream.name}: ${streamTotals[stream]} </br>
            </g:each>
        </div>

    </aside>

    <section id="main" class="span9">

        <div class="row-fluid">
            <table id="primary-table" class="table table-striped tablesorter">
                <thead>
                    <tr>
                        <th>Rank</th>
                        <th>Platform</th>
                        <th>Channel</th>
                        <th>Game</th>
                        <th>Viewers</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    <g:each in="${streams}" status="i" var="stream">
                        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                            <td>${i + 1}</td>
                            <td>${stream.service}</td>
                            <td><a href="${stream.url}">${stream.channel}</a></td>
                            <td>${stream.game}</td>
                            <td>${stream.viewers}</td>
                            <td>${stream?.broadcastName?.take(40)} <g:if test="${stream?.broadcastName?.size() > 40}">...</g:if></td>
                        </tr>
                    </g:each>
                </tbody>
            </table>
        </div>


    </section>
</div>



</body>
</html>
