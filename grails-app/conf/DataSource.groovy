dataSource {
    pooled = true
    driverClassName = "com.mysql.jdbc.Driver"
    username = "root"
    password = ""
    properties {
        minEvictableIdleTimeMillis=1800000
        timeBetweenEvictionRunsMillis=1800000
        numTestsPerEvictionRun=3
        testOnBorrow=false
        testWhileIdle=true
        testOnReturn=false
        validationQuery="SELECT 1"
        maxActive=20
    }
}
hibernate {
    cache.use_second_level_cache = false
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}


environments {
    development {
        dataSource {
            // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:mysql://localhost/streams"
            dbCreate = "update"
            username = "root"
            password = "root"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            username = "streams"
            password = "fne4oi4#nsda#oi!3d32"
            url = "jdbc:mysql://localhost/streams"
        }
    }
    production {
        dataSource {
            dbCreate = "validate"
            username = "streams"
            password = "fne4oi4#nsda#oi!3d32"
            url = "jdbc:mysql://localhost/streams"
        }
    }
}