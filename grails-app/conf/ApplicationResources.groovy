modules = {
    application {
        resource url:'js/application.js'
    }

    tablesorter {
        dependsOn 'jquery'
        resource url: '/js/jquery.tablesorter.min.js'
    }
}