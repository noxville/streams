package streams;

class StreamUpdateJob {

    def concurrent = false

    def streamUpdateService

    static triggers = {
        simple name:'streamUpdateTrigger', startDelay: 1000, repeatInterval: 5L * 60 * 1000
    }

    def execute(){
        streamUpdateService.update()
    }
}